package com.codewithbisky.user.model;

public enum StorageProvider {
    BUNNY_NET,
    S3
}
