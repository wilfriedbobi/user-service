package com.codewithbisky.user.repository;

import com.codewithbisky.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,String> {
}
